FROM java:8

WORKDIR /minecraft

COPY files/eula.txt /minecraft/eula.txt

RUN wget https://media.forgecdn.net/files/3123/982/Valheslsia_SERVER-pre7-3.1.0.zip \
    && unzip Valhelsia_SERVER-pre7-3.1.0.zip \
    && rm Valhelsia_SERVER-pre7-3.1.0.zip
RUN chmod u+x Install.sh ServerStart.sh
RUN sed -i '2i /bin/sh /minecraft/CheckEula.sh' /minecraft/ServerStart.sh
RUN /minecraft/Install.sh

ENV EULA="true"
